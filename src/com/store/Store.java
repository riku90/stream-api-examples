package com.store;

import com.store.items.base.SaleableItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Store {
    private List<SaleableItem> items;

    public Store() {
    }

    public Store(List<SaleableItem> items) {
        this.items = items;
    }

    public <T> List<T> getItems(Class<T> tClass) {
        /*List<T> result = new ArrayList<>();
        for (SaleableItem item : items) {
            if (item.getClass() == tClass)
                result.add((T) item);
        }

        return result;*/

        return items.stream()
                .filter(t -> t.getClass() == tClass)
                .map(t -> (T) t)
                .collect(Collectors.toList());
    }

    public double totalPrice() {
        /*double total = 0;
        for (SaleableItem item : items) {
            total += item.price();
        }

        return total;*/

        return items.stream()
                .map(SaleableItem::price)
                .reduce(Double::sum)
                .orElse(0.0);
    }

    public Map<String, List<SaleableItem>> itemsByType() {
        /*Map<String, List<SaleableItem>> result = new HashMap<>();
        for (SaleableItem item : items) {
            if (!result.containsKey(item.getClass().getSimpleName()))
                result.put(item.getClass().getSimpleName(), new ArrayList<>());
            result.get(item.getClass().getSimpleName()).add(item);
        }

        return result;*/

        return items.stream()
                .collect(Collectors.groupingBy(t -> t.getClass().getSimpleName()));
    }

    public List<SaleableItem> getItems() {
        return items;
    }
}
