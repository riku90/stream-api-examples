package com.store.dto;

public class ItemDTO {
    private String type;
    private String madeBy;
    private double price;

    public ItemDTO(String type, String madeBy, double price) {
        this.type = type;
        this.madeBy = madeBy;
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMadeBy() {
        return madeBy;
    }

    public void setMadeBy(String madeBy) {
        this.madeBy = madeBy;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return String.format("{%s,%s,%s}", type, madeBy, price);
    }
}
