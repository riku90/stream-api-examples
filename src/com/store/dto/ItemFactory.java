package com.store.dto;

import com.store.items.Box;
import com.store.items.Cup;
import com.store.items.base.SaleableItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ItemFactory {
    public static ItemDTO from(Cup cup) {
        return new ItemDTO(cup.getClass().getSimpleName(), cup.getMadeBy(), cup.price());
    }

    public static ItemDTO from(Box box) {
        return new ItemDTO(box.getClass().getSimpleName(), box.getMadeBy(), box.price());
    }

    public static List<ItemDTO> from(List<SaleableItem> items) {
        /*List<ItemDTO> result = new ArrayList<>();
        for (SaleableItem item: items) {
            ItemDTO itemDTO = from(item);
            if (Objects.nonNull(itemDTO))
                result.add(itemDTO);
        }

        return result;*/

        return items.stream()
                .map(ItemFactory::from)
                .collect(Collectors.toList());
    }

    public static ItemDTO from(SaleableItem item) {
        if (item instanceof Cup)
            return from((Cup) item);
        else if (item instanceof Box)
            return from((Box) item);
        return null;
    }
}
