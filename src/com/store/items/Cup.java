package com.store.items;

import com.store.items.base.SaleableItem;

public class Cup implements SaleableItem {
    private Color color;
    private double price;
    private int size;
    private String madeBy;

    public Cup(Color color, double price, int size, String madeBy) {
        this.color = color;
        this.price = price;
        this.size = size;
        this.madeBy = madeBy;
    }

    @Override
    public double price() {
        return price;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getMadeBy() {
        return madeBy;
    }

    public void setMadeBy(String madeBy) {
        this.madeBy = madeBy;
    }

    @Override
    public String toString() {
        return String.format("{%s,%s,%s,%s}", color, size, madeBy, price);
    }
}
