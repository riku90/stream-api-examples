package com.store.items.base;

public interface SaleableItem {
    double price();
}
