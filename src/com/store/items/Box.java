package com.store.items;

import com.store.items.base.SaleableItem;

public class Box implements SaleableItem {
    private double price;
    private double height;
    private double width;
    private String madeBy;

    @Override
    public double price() {
        return price;
    }

    public Box(double price, double height, double width, String madeBy) {
        this.price = price;
        this.height = height;
        this.width = width;
        this.madeBy = madeBy;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public String getMadeBy() {
        return madeBy;
    }

    public void setMadeBy(String madeBy) {
        this.madeBy = madeBy;
    }

    @Override
    public String toString() {
        return String.format("{%s,%s,%s,%s}", height, width, madeBy, price);
    }
}
